---
layout: handbook-page-toc
title: "Acquisition Performance Indicators"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Acquisition impact

Acquisition impact measures the number of [product categories](https://about.gitlab.com/direction/maturity/#category-maturity) which reached Complete [maturity](https://about.gitlab.com/direction/maturity/), or higher, as a result of acquisitions. The target is 3 per year.

## Acquisition success

Acquisition success measures how successful GitLab at integrating the new teams and technologies acquired. An acquisition is a success if it ships 70% of the acquired company's product functionality as part of Gitlab within three months after acquisition. The acquisition success rate is the percentage of acquired companies that were successful. The target is 70% success rate.

## Sourced pipeline

The sourced pipeline includes companies operating in a DevOps segment relevant for GitLab. Companies have developed or are working on product functionality which is either:
   1. A new category on our roadmap or
   1. At a more advanced maturity than our current categories

Target is ongoing monitoring of at least 1000 companies.