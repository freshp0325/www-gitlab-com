---
layout: handbook-page-toc
title: "SG.1.02 - Exception Management"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# SG.1.02 - Exception Management

## Control Statement

GitLab reviews exceptions to policies, standards, and procedures; exceptions are documented and approved based on business need and removed when no longer required.

## Context

The purpose of this control is to ensure that exceptions to GitLab's policies, standards, and procedures, are documented and approved based on business need and maintained until no longer required. This is accomplished through the enforcement of the [Information Security Policy Exception Management Process](https://about.gitlab.com/handbook/engineering/security/#information-security-policy-exception-management-process). As part of iniating a Security Policy Exception Request, the requester is required to provide information such as the scope of the exception being requested (e.g. specific policies an exception is being requested for), the length of time for the exception, any business justifications and alternative solutions that could be used, and any compensating controls or other internal process that reduce the risk of the exception being requested. Once this information is provided, an approval is required from specific individuals based on the type of data impacted per [GitLab's Data Classification Policy](https://about.gitlab.com/handbook/engineering/security/data-classification-policy.html). The approval matrix is included as part of the [Policy Exception Request Issue Template](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).

## Scope

Any deviations from GitLab's policies, standards, and procedures, are required to undergo a formal exception process.

## Ownership

Control Owner:

* Security

Process Owner:

* Security

## Guidance

As exceptions arise due to a deviation from GitLab's policies, the individual requiring an exception request is tasked with submitting the request for approval.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Exception Management control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/876).

### Policy Reference
- [Information Security Policy Exception Management Process](https://about.gitlab.com/handbook/engineering/security/#information-security-policy-exception-management-process)

## Framework Mapping

* ISO
   * A.5.1.1