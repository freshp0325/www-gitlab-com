require 'spec_helper'

describe MergeCheck do
  describe '.wait_for_mergeability' do
    subject { described_class.wait_for_mergeability(merge_request, delay) }
    let(:delay) { 0 }

    context 'when merge request can be merged' do
      let(:merge_request) { double(merge_status: 'can_be_merged') }

      it { is_expected.to be_truthy }
    end

    context 'when merge request is not mergable yet' do
      let(:merge_request) { double(merge_status: 'checking', project_id: 1, iid: 2) }

      before do
        expect(Gitlab).to receive(:merge_request).with(1, 2) { double(merge_status: 'can_be_merged') }
      end

      it 'refreshes merge request data and does another check' do
        is_expected.to be_truthy
      end
    end

    context 'when merge request is not mergable after 10 attempts' do
      let(:merge_request) { double(merge_status: 'checking', project_id: 1, iid: 2) }

      before do
        expect(Gitlab).to receive(:merge_request).with(1, 2).exactly(10).times.and_return(merge_request)
      end

      it 'stops trying and returns false' do
        is_expected.to be_falsey
      end
    end
  end
end
