# frozen_string_literal: true

module MergeCheck
  RETRY_DELAY = 1
  RETRY_MAX = 10

  # With async mergeability checks, we need to wait for a newly created MR to be actually
  # mergeable, otherwise the accept_merge fails
  def self.wait_for_mergeability(merge_request, delay = RETRY_DELAY)
    retry_count = 0
    while %w[checking unchecked].include?(merge_request.merge_status) && retry_count < RETRY_MAX
      sleep(delay)
      merge_request = Gitlab.merge_request(merge_request.project_id, merge_request.iid)
      retry_count += 1
    end

    retry_count != RETRY_MAX
  end
end
